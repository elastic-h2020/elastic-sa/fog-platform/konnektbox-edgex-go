//
// Copyright (c) 2017
// Cavium
// Mainflux
// IOTech
//
// SPDX-License-Identifier: Apache-2.0
//

package distro

import (
	"context"
	"crypto/tls"
	"fmt"
	"strconv"
	"strings"
	"io/ioutil"
	"crypto/x509"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	contract "github.com/edgexfoundry/go-mod-core-contracts/models"
)

type mqttSender struct {
	client MQTT.Client
	topic  string
}

// newMqttSender - create new mqtt sender
func newMqttSender(addr contract.Addressable, cert string, key string) sender {
	protocol := strings.ToLower(addr.Protocol)

	opts := MQTT.NewClientOptions()
	broker := protocol + "://" + addr.Address + ":" + strconv.Itoa(addr.Port) + addr.Path
	opts.AddBroker(broker)
	opts.SetClientID(addr.Publisher)
	opts.SetUsername(addr.User)
	opts.SetPassword(addr.Password)
	opts.SetAutoReconnect(false)

	if protocol == "tcps" || protocol == "ssl" || protocol == "tls" {
		cert, err := tls.LoadX509KeyPair(cert, key)

		if err != nil {
			LoggingClient.Error(fmt.Sprintf("Failed loading x509 data: %v", err))
			return nil
		}

		// Add support for custom CA
		certpool := x509.NewCertPool()
		pem, err := ioutil.ReadFile("/certs/ca.pem")
		if err != nil {
			LoggingClient.Error(fmt.Sprintf("Failed to read client certificate authority: %v", err))
		}
		if !certpool.AppendCertsFromPEM(pem) {
			LoggingClient.Error("Can't parse client certificate authority")
		}

		tlsConfig := &tls.Config {
			ClientCAs:          certpool,
			InsecureSkipVerify: true,
			Certificates:       []tls.Certificate{cert},
		}

		opts.SetTLSConfig(tlsConfig)

	}

	sender := &mqttSender{
		client: MQTT.NewClient(opts),
		topic:  addr.Topic,
	}

	return sender
}

func (sender *mqttSender) Send(data []byte, ctx context.Context) bool {
	if !sender.client.IsConnected() {
		LoggingClient.Info("Connecting to mqtt server")
		if token := sender.client.Connect(); token.Wait() && token.Error() != nil {
			LoggingClient.Error(fmt.Sprintf("Could not connect to mqtt server, drop event. Error: %s", token.Error().Error()))
			return false
		}
	}

	token := sender.client.Publish(sender.topic, 0, false, data)
	// FIXME: could be removed? set of tokens?
	token.Wait()
	if token.Error() != nil {
		LoggingClient.Error(token.Error().Error())
		return false
	} else {
		LoggingClient.Debug(fmt.Sprintf("Sent data: %X", data))
		return true
	}
}

#!/bin/sh
docker build \
	-f cmd/export-distro/Dockerfile \
	--label "git_sha=1c2449f73fa961aa7634dfaed45422a7c979d68e" \
	-t edgexfoundry/docker-export-distro-go:1c2449f73fa961aa7634dfaed45422a7c979d68e \
	-t edgexfoundry/docker-export-distro-go:1.0.0-dev \
	-t registry.bda.ikerlan.es/konnektbox/docker-export-distro-go:1.0.0 \
	.

docker build \
	-f cmd/config-seed/Dockerfile \
	--label "git_sha=656cd6f2913cd0a8d7ae3f56d7fc90a8aedc3ae2" \
	-t edgexfoundry/docker-core-config-seed-go:656cd6f2913cd0a8d7ae3f56d7fc90a8aedc3ae2 \
	-t edgexfoundry/docker-core-config-seed-go:1.0.0-dev \
	-t registry.bda.ikerlan.es/konnektbox/docker-core-config-seed-go:1.0.0 \
	.
